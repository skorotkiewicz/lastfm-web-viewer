<?php require_once 'libs/functions.php'; ?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Latest music activity (@<?=$user?>)">
    <link rel="shortcut icon" href="styles/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="styles/lastfm.png" sizes="192x192">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
    <link href="styles/offcanvas.css" rel="stylesheet">
    <meta name="author" content="@<?=$user?>">
    <title>Latest music activity (@<?=$user?>)</title>
  </head>

  <body class="bg-light">

    <main role="main" class="container">

        <div class="d-flex align-items-center p-3 my-3 text-white-50 bg-grey-dark rounded box-shadow">   
            <img class="mr-3 rounded" src="<?=$avatar?>" alt="" width="48" height="48">
            <div class="lh-100">
            <h6 class="mb-0 text-white lh-100"><?=$fullname?> (@<?=$user?>)</h6>
            <small>Lastfm Timeline</small>
            </div>
        </div>

        <div class="my-3 p-3 bg-white rounded box-shadow">
            <h6 class="border-bottom border-gray pb-2 mb-0">Latest music activity</h6>

            <?php foreach($fm['recenttracks']['track'] as $item): ?>
            <div class="media text-muted pt-3">
            <a href="<?=$item['url']?>"><img src="<?=$item['image'][0]['#text'] ? $item['image'][0]['#text'] : 'styles/favicon.ico';?>" alt="" class="mr-2 rounded"></a>
            <div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                <div class="d-flex justify-content-between align-items-center w-100">
                    <strong class="text-gray-dark"><?=$item['name']?></strong>
                    <?=$item['date']['uts'] ? time_elapsed_string($item['date']['uts']) : '<span><img src="styles/eq_icon.gif" /> Scrobbling now</span>';?>
                </div>
                <span class="d-block"><?=$item['artist']['#text']?></span>
            </div>
            </div>
            <?php endforeach; ?>

            <small class="d-block text-right mt-3">
            <a href="https://www.last.fm/user/<?=$user?>">All Scrobbles</a>
            </small>
        </div>

      <div class="alert alert-light" role="alert">
        <tt>made with ❤︎ by <a href="https://twitter.com/skorotkiewicz">skorotkiewicz</a> / source code <a href="https://gitlab.itunix.eu/skorotkiewicz/lastfm-web-viewer">here</a></small>
             / cached <?php echo time() - filemtime($filename);?>s ago
             <?php
                $time = explode(' ', microtime());
                $time = $time[1] + $time[0];
                $finish = $time;
                $total_time = round(($finish - $start), 4);
                echo '<br />Page generated in '.$total_time.' seconds.';
             ?>
        </tt>
      </div>
      
    </main>

  </body>
</html>