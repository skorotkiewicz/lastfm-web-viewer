<?php
require_once 'config.php';

// Measuring PHP Page Load Time
$time = explode(' ', microtime());
$time = $time[1] + $time[0];
$start = $time;

$filetime = file_exists($filename) ? filemtime($filename) : time() - $caching - 1;

if (time() - $caching > $filetime) {
    $json = file_get_contents("http://ws.audioscrobbler.com/2.0/?method=user.getrecenttracks&user=$user&api_key=$api&format=json");
    file_put_contents($filename, $json);
} else {
	$json = file_get_contents($filename);
}
$fm = json_decode($json, true);


function time_elapsed_string($ptime) {
    $etime = time() - $ptime;

    if ($etime < 1) {
        return '0 seconds';
    }

    $a = array( 365 * 24 * 60 * 60  =>  'year',
                 30 * 24 * 60 * 60  =>  'month',
                      24 * 60 * 60  =>  'day',
                           60 * 60  =>  'hour',
                                60  =>  'minute',
                                 1  =>  'second'
                );
    $a_plural = array( 'year'   => 'years',
                       'month'  => 'months',
                       'day'    => 'days',
                       'hour'   => 'hours',
                       'minute' => 'minutes',
                       'second' => 'seconds'
                );

    foreach ($a as $secs => $str) {
        $d = $etime / $secs;
        if ($d >= 1) {
            $r = round($d);
            return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str) . ' ago';
        }
    }
}

?>